+++
title = "About Welfareness"
date = "2020-07-30T06:01:00+08:00"
slug = "about"
aliases = ["aboutus"]
#translationKey = "about"
#draft = true
image = 'read.jpg'
+++

**Welfareness** is a website and webblog about our welfareness. It is defined as being at peace and living happily—from our physical body, to our mind, and financial status. It is about being fit, healthy, and well on one side; and having financial stability and ultimately freedom and wealth on the other.

## Welfareness Hubs

### Health Welfareness
**[Health Welfareness](https://health.welfareness.icu)** is our hub for food, fitness, and overall health. We discuss about clean real food which brings joy and healing to our body. We also talk about how to be fit by doing exercises and workouts.

### Wealth Welfareness
**[Wealth Welfareness](https://wealth.welfareness.icu)** is the place where discussions on how to achieve financial stability is at, with the ultimate goal in achieving financial freedom. It include the basics like how to budget, how to be free from our spending habits, to advance topics like starting a business and investing.

### COVID-19 Welfareess
**[COVID-19 Welfareness](https://covid19.welfareness.icu)** is our center for information about the COVID-19, the disease caused by the SARS-CoV-2 virus. Links to important research and study papers are provided together with links to reputable news sources.

___

**Welfareness** is my journey into achieving wellness and financial freedom.
